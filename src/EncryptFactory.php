<?php

declare(strict_types=1);

namespace assist;

use assist\encrypt\Encrypt;

/**
 * Class EncryptFactory
 * @package assist
 */
class EncryptFactory
{


    /**
     * 加解密对象
     * @param string $type
     * @return Encrypt
     * @throws \Exception
     */

    public static function handle(string $type=null):Encrypt
    {

        $encryptType=is_null($type)?(PHP_VERSION_ID>=50500?'bcrypt':'md5'):$type;

        $class = "assist\\encrypt\\" . ucfirst($encryptType);

        if (!class_exists($class)) {
            throw new \Exception("Not found {$encryptType} hash type!");
        }

        return new $class();
    }

}