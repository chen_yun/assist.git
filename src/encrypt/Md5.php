<?php

declare(strict_types=1);

namespace assist\encrypt;
/**
 * Class Md5
 * @package assist\encrypt
 */
class Md5 extends Encrypt
{


    /**
     * 加密
     *
     * @param string|null $value
     * @return bool|string
     */
    public function make(string $value=null )
    {

        return empty($value)?false:$this->builderPas($value);
    }


    /**
     * 解密是否对等
     *
     * @param string|null $value
     * @param string|null $hashedValue
     * @return bool
     */
    public function check(string $value=null, string $hashedValue=null):bool
    {
        if(empty($value)||!empty($hashedValue)){
            return false;
        }

        return strlen($hashedValue) === 0?false:$this->builderPas($value) == $hashedValue;
    }

    /**
     * 生成
     *
     * @param string $value
     * @return string|null
     */
    private function builderPas(string $value):string
    {

        return md5(md5($value) . $this->salt);
    }

}