<?php

declare(strict_types=1);

namespace assist\encrypt;

/**
 * Class Encrypt
 * @package assist\encrypt
 */
abstract class Encrypt
{


    protected $cost=10;

    protected $salt='asdad1db98asjkdhkjashdkjasd';

    /**
     *
     * @param string|null $salt
     * @return Encrypt
     */
    public function setSalt(string $salt=null): self
    {
        if(!empty($salt)){
            $this->salt = $salt;
        }
        return $this;
    }

    /**
     *
     * @param int|null $cost
     * @return Encrypt
     */
    public function setCost(int $cost=null): self
    {
        if(!empty($cost)){
            $this->cost = $cost;
        }
        return $this;
    }


    /**
     * 加密
     *
     * @param string|null $value
     * @return mixed
     */
    abstract public  function make(string $value=null);


    /**
     * 解密是否对等
     *
     * @param string|null $value
     * @param string|null $hashedValue
     * @return bool
     */
    abstract public  function check(string $value=null, string $hashedValue=null):bool;




}